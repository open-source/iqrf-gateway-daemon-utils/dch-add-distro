#!/usr/bin/python3

"""
Copyright 2019-2024 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from setuptools import setup
from dch_add_distro import __version__

with open('README.md') as stream:
    long_description = stream.read()

with open('requirements.txt', 'r') as requirements_file:
    requirements = requirements_file.read().splitlines()

setup(
    name='dch_add_distro',
    description='Python utility for adding name and version of Debian-based distributions into the changelog.',
    long_description=long_description,
    version=__version__,
    license='Apache-2.0',
    author='Roman Ondráček',
    author_email='roman.ondracek@iqrf.com',
    url='https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon-utils/dch-add-distro',
    packages=['dch_add_distro'],
    entry_points={
        'console_scripts': [
            'dch-add-distro = dch_add_distro.main:main',
        ]
    },
    install_requires=requirements
)
