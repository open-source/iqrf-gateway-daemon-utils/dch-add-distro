/**
 * Copyright 2019-2024 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

[
  { name: 'debian', version: 'buster', arch: 'all' },
  { name: 'debian', version: 'bullseye', arch: 'all' },
  { name: 'debian', version: 'bookworm', arch: 'all' },
  { name: 'ubuntu', version: 'bionic', arch: 'all' },
  { name: 'ubuntu', version: 'focal', arch: 'all' },
  { name: 'ubuntu', version: 'jammy', arch: 'all' },
  { name: 'ubuntu', version: 'noble', arch: 'all' },
]
