/**
 * Copyright 2019-2024 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

{
  // Base Docker image
  baseImage: 'iqrftech/debian-python-builder',
  // Directory for package deployment
  deployDir: '/data/nginx/dl/dch-add-distro/${DIST}/${ARCH_PREFIX}${ARCH}/${STABILITY}/',
  // Commands for package build
  packageBuildCommands(distribution, stability): [
    'python3 -m venv venv',
    'source venv/bin/activate',
    'pip3 install -r requirements.txt',
    'pip3 install setuptools',
    'python3 setup.py install',
    'dch-add-distro -d "${DIST}" -D "${DATE}"',
    'debuild -e CI_PIPELINE_ID -b -us -uc -tc',
  ],
}
