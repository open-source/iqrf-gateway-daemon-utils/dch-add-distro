"""
Copyright 2019-2024 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


class Distribution:
    """Debian-based distribution and its version value object."""

    def __init__(self, code_name: str):
        """
        Construct debian-based distribution value object.

        :param code_name: Distribution version code name
        :type code_name: str
        """
        debian_versions = {
            'stretch': '9',
            'buster': '10',
            'bullseye': '11',
            'bookworm': '12',
            'trixie': '13',
            'forky': '14',
        }
        ubuntu_versions = {
            'xenial': '16.04',
            'bionic': '18.04',
            'disco': '19.04',
            'eoan': '19.10',
            'focal': '20.04',
            'groovy': '20.10',
            'hirsute': '21.04',
            'impish': '21.10',
            'jammy': '22.04',
            'kinetic': '22.10',
            'lunar': '23.04',
            'mantic': '23.10',
            'noble': '24.04',
            'oracular': '24.10',
        }
        self.__code_name = code_name
        if code_name in debian_versions:
            self.__distribution = 'debian'
            self.__version = debian_versions[code_name]
            return
        if code_name in ubuntu_versions:
            self.__distribution = 'ubuntu'
            self.__version = ubuntu_versions[code_name]
            return
        raise NotImplementedError('Unsupported distribution.')

    def code_name(self) -> str:
        """
        Return distribution version code name.

        :return: Distribution version code name
        :rtype: str
        """
        return self.__code_name

    def distribution(self) -> str:
        """
        Return distribution name.

        :return: Distribution name
        :rtype: str
        """
        return self.__distribution

    def suffix(self) -> str:
        """
        Return package version suffix.

        :return: Package version suffix
        :rtype: str
        """
        return self.__distribution + self.__version

    def version(self) -> str:
        """
        Return distribution version.

        :return: Distribution version
        :rtype: str
        """
        return self.__version
