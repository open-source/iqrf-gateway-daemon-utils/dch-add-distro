#!/usr/bin/python3

"""
Copyright 2019-2024 MICRORISC s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
import sys

from typing import Optional

from debian.changelog import Changelog, Version

from .distribution import Distribution
from . import __version__


def main() -> int:
    """
    Run main point of the application.

    :return: Execution status
    :rtype: int
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', '--file', action='store', type=str, dest='filePath',
        default='debian/changelog', help='Debian changelog file path'
    )
    parser.add_argument(
        '-d', '--distribution', action='store', type=str, dest='code_name',
        help='Code name of the target distribution', required=True
    )
    parser.add_argument(
        '-D', '--date', action='store', type=str, dest='date',
        help='Date of the latest change block', required=False, default=None
    )
    parser.add_argument(
        '-v', '--version', action='version', version='%(prog)s v' + __version__
    )
    args = parser.parse_args()
    code_name = args.code_name
    date = args.date
    with open(args.filePath, 'r+', encoding='utf-8') as file:
        changelog = Changelog(file=file, encoding='utf-8')
        edit_changelog(changelog, code_name, date)
        file.seek(0)
        file.write(str(changelog))
        file.truncate()
    return 0


def edit_changelog(
        changelog: Changelog,
        code_name: str,
        date: Optional[str]
) -> None:
    """
    Change the version and the distribution in changelog.

    :param changelog: Changelog object
    :type changelog: Changelog
    :param code_name: Distribution's code name
    :type code_name: str
    :param date: Latest change block date
    :type date: str|None
    """
    distribution = Distribution(code_name)
    for block in changelog:
        version = f'{block.version.full_version}+{distribution.suffix()}'
        block.version = Version(version)
        block.distributions = code_name
    if date is not None:
        changelog.date = date


if __name__ == '__main__':
    sys.exit(main())
